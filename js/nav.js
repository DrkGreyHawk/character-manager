window.onload = navigator;

function navigator(){
  document.getElementById('navMenu').innerHTML = 
    '<div class="container-fluid">' +
      '<div class="navbar-header">' +
        '<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">' +
          '<span class="icon-bar"></span>' +
          '<span class="icon-bar"></span>' +
          '<span class="icon-bar"></span>' +
        '</button>' +
        '<a class="navbar-brand" href="index.html">DrkGreyHawk</a>' +
      '</div>' +
      '<div class="collapse navbar-collapse" id="myNavbar">' +
        '<ul class="nav navbar-nav">' +
          '<li><a href="index.html">Home</a>' +
          '</li>' +
          '<li><a href="projects.html">Projects</a>' +
          '</li>' +
        '</ul>' +
        '<ul class="nav navbar-nav navbar-right">' +
          '<li><a href="sign_up.html"><span class="glyphicon glyphicon-user"></span> Sign Up</a>' +
          '</li>' +
          '<li><a href="login.html"><span class="glyphicon glyphicon-log-in"></span> Login</a>' +
          '</li>' +
        '</ul>' +
      '</div>' +
    '</div>'
}