// Populates the <select> tags from the data.json file
// This seems redundant, is there a way to minimize this?
$.getJSON("../json/harn_data.json", function (data) {

})
.fail(function (jqxhr, status, error) { 
  console.log('error', status, error);
});

function clearResults(){
  $( '#theResults' ).empty();
}

// MODAL START
var modal = document.getElementById('resultsModal');
var span = document.getElementsByClassName('close')[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function(){
  modal.style.display = 'none';
  clearResults();
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = 'none';
    //clearResults();
  }
};

function generateModal(){
  modal.style.display = 'block';
}

function cancelCharacter(){
  modal.style.display = 'none';
  clearResults();
}
// MODAL END

function fleshOutCharacter( data ) {
  
  
}

// Runs when the submit button is pressed for character_input.html
function character_inputSubmit() {
  $( '#inputCharacter' ).submit(function( event ) {
    var submitted = $( this ).serializeArray();

      console.log( submitted );
      event.preventDefault();

      fleshOutCharacter(submitted);
      generateModal();
      
      //DEBUGGING
      $( '#theResults' ).html('');

      for (var i = 0; i < submitted.length; i++) {
        $( '#theResults' ).append(submitted[i]['name'] + ': ' + submitted[i]['value'] + '</br>');
      }
  });
}

function finalizeCharacter(){
  window.location.href = 'index.html';
}