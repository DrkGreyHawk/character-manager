/* script.js DEPRECATED
 * Created by: Hyrum Mack
 * DrkGreyHawk.com 
 *
 * NOTE: DEPRECATED as of 02:47 07/02/16
 * NOTE: Client side script form validation should be avoided at all times.
 * NOTE: This was a great learning tool though, I learned a lot about Javascript and JQUERY.
 * NOTE: Get better at being efficient with your Javascript. You can't rely on third party apps/applets/code to clean up your messy code.
 */

var errorNum = [];
var errorFree = [];
var errorReportNumber;
//var meleeErrorDiv is called in weapon_matrices.html

function cloneRow(tableName, rowName, increment) {
  changeCellName(rowName, increment);
  var row = document.getElementById(rowName); //Find row to copy
  var table = document.getElementById(tableName); //Find table to append to
  var clone = row.cloneNode(true); //Copy children too
  table.appendChild(clone); //Add new row to end of table
}

function setErrorReport(div, $div, $input, errorText, style, num) {
  errorNum[num] = errorText;
  $($input).css('background-color','pink');
  div.innerHTML = cleanArray(errorNum).toString().replace(/,/g, '<br/>');
  $($div).addClass(style);
  console.log('ERROR! code: ' + num + ' "' + errorText + '"');
}

function clearErrorReport(div, $div, $input, style, num){
  errorNum[num] = undefined;
  $($input).css('background-color','white');
  //Check to find if the array is empty
  for (var i = 0; i < errorNum.length; i++) {
    if (errorNum[i] !== undefined) {
      isEmpty = false;
      break;
    } else {
      isEmpty = true;
    }
  }
  
  if (isEmpty === false) {
    div.innerHTML = cleanArray(errorNum).toString().replace(/,/g, '<br/>');
  } else {
    div.innerHTML = cleanArray(errorNum).toString().replace(/,/g, '<br/>');
    $($div).removeClass(style);
  }
}

//cleans up undefined elements from the array I LOVE THIS BIT OF CODE SO MUCH... so freaking much. Thanks to http://stackoverflow.com/users/5445/cms
function cleanArray(actual) {
  var newArray = new Array();
  for (var i = 0; i < actual.length; i++) {
    if (actual[i]) {
      newArray.push(actual[i]);
    }
  }
  return newArray;
}

//MELEE 
function populateMelee() {
  
  var meleeRows = document.getElementById('melee_table').rows.length;
  var meleeCount = $('input[name="melee_input"]').val(); //Count how many the user wants

  //Clear table befor cloning.
  $('#melee_input_row input').val('');

  //Validate input
  //ERROR:885
  errorReportNumber = 885;
  if (meleeCount < 11 && meleeCount > 0) {
    clearErrorReport(meleeErrorDiv, '#melee_error', 'input[name="melee_input"]', 'error_report', errorReportNumber);
    
    //Clear all but first two rows  
    for (i = meleeRows; i > 2; i--) {
      var x = i - 1;
      document.getElementById('melee_table').deleteRow(x);
    }
    
    //Clone and paste cloned row nth times
    meleeCount -= 1;
    for (i = 0; i < meleeCount; i++) {
      cloneRow('melee_table', 'melee_input_row', i);
    }
  } else {
    setErrorReport(meleeErrorDiv, '#melee_error', 'input[name="melee_input"]', 'Enter a digit between 1 and 10', 'error_report', errorReportNumber);
  }
}


//TODO: Find a way to change the name of the cells so they can be called individually.
function changeCellName(rowName, increment) {
  row = document.getElementById(rowName) + increment.toString(); // Find row and add the increment to the end of it.
  console.log(row);
  cell = $('input[name|="melee"]').attr('name', function() {return $(this).attr('')}); // Change the 
}



function submitMelee() {
  
  //Validate the form
  errorFree = [];
  //Check for empty cells
  //ERROR:0
  errorReportNumber = 0;
  $('#melee_table :input[required="required"]').each(function() {
    if ($.trim($(this).val()).length === 0) {
      setErrorReport(meleeErrorDiv, '#melee_error', undefined, '[EMPTY VALUE] All fields must have an input', 'error_report', errorReportNumber);
      errorFree[0] = false;
    } else {
      clearErrorReport(meleeErrorDiv, '#melee_error', undefined, 'error_report', errorReportNumber);
      errorFree[0] = true;
    }
  });
      
  
  //Size
  //ERROR:1
  errorReportNumber = 1;
  var errorCheck = $('input[name="melee_size"]').val().toUpperCase();
  
  if (errorCheck === 'L' || errorCheck === 'M' || errorCheck === 'H') {
    clearErrorReport(meleeErrorDiv, '#melee_error', 'input[name="melee_size"]', 'error_report', errorReportNumber);
    //$('input[name="melee_size"]').val(errorCheck); //Makes sure the value is in caps
    errorFree[1] = true;
  } else {
    setErrorReport(meleeErrorDiv, '#melee_error', 'input[name="melee_size"]', '[SIZE] Must be L/M/H', 'error_report', errorReportNumber);
    errorFree[1] = false;
  }
  
  //Hands
  //ERROR:2
  errorReportNumber = 2;
  errorCheck = parseInt($('input[name="melee_hands"]').val());
  if (errorCheck === 1 || errorCheck === 2) {
    clearErrorReport(meleeErrorDiv, '#melee_error', 'input[name="melee_hands"]', 'error_report', errorReportNumber); 
    errorFree[2] = true;
  }
  else {
    setErrorReport(meleeErrorDiv, '#melee_error', 'input[name="melee_hands"]', '[HANDS] Must be the number 1 or 2', 'error_report', errorReportNumber);
    errorFree[2] = false;
  }
  
  //Reach
  //ERROR:3
  errorReportNumber = 3;
  cell5E = parseInt($('input[name="melee_reach"]').val());
  errorCheck = isNaN(cell5E);
  if (errorCheck === true) {
    setErrorReport(meleeErrorDiv, '#melee_error', 'input[name="melee_reach"]', '[REACH] Must be a number between 1 & 10', 'error_report', errorReportNumber); //isNaN
    errorFree[3] = false;
  } else {
    clearErrorReport(meleeErrorDiv, '#melee_error', 'input[name="melee_reach]', 'error_report', errorReportNumber);
    errorFree[3] = true;
    if (cell5E < 1 || cell5E > 10) {
      setErrorReport(meleeErrorDiv, '#melee_error', 'input[name="melee_reach"]', '[REACH] Must be between 1 & 10', 'error_report', errorReportNumber); //too high or low
      errorFree[3] = false;
    }
  } if (errorFree[3] === true) { clearErrorReport(meleeErrorDiv, '#melee_error', 'input[name="melee_reach]', 'error_report', errorReportNumber); }

  //Speed
  //ERROR: 4
  errorReportNumber = 4;
  

  /*
  *Check for errors
  */
  var sendIt = false;
  for (var i = 0; i < errorFree.length; i++) {
    
    if (errorFree[i] === false) {
      sendIt = false;
      break;
    }
    else {
      sendIt = true;
    }
    //Break loop if there is an error
  }
  
  //Submits if there are no errors
  if (sendIt === true) {document.getElementById('melee_weapons_form').submit();}
  
  
  //DEBUG: Display the contents of a column TODO REMOVE THIS WHEN FINISHED
  var values = [];
  $("input[name='melee_size']").each(function() {
    values.push($(this).val());
  });
  //window.alert(values);
}

//RANGED

function submitRanged() {
  window.alert('TODO: submitRanged() - PHP Validation');
}

function populateRanged() {
  
}